using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace Repositorios{

    public class AzureConsultorioRepository : IConsultorioRepository
    {
         private string conexion;

        public AzureConsultorioRepository(string azAccountConnection)
        {
            conexion = azAccountConnection;
        }

        private CloudTable ObtenerTablaAzure(){
            // Retrieve the storage account from the connection string.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(conexion);

            // Create the table client.
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();

            // Retrieve a reference to the table.
            CloudTable table = tableClient.GetTableReference("Consultorios");

            // Create the table if it doesn't exist.
           // table.CreateIfNotExistsAsync().GetAwaiter();
            return table;
        }
        public Task<bool> Actualizar(ConsultorioIdentity actualizado)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Borrar(string id)
        {
            throw new NotImplementedException();
        }

        public async Task<bool>  Guardar(ConsultorioIdentity nuevo)
        {
            var table = ObtenerTablaAzure();

             var azE = new ConsultorioAzureEntity(nuevo.Identificador, nuevo.Id);
                azE.Numero = nuevo.Numero;
                var insertOperation = TableOperation.Insert(azE);
               await table.ExecuteAsync(insertOperation);
               return true;
        }

        public  async Task<ConsultorioIdentity> LeerPorId(String id)
        {
            var table = ObtenerTablaAzure();

            TableOperation retrieveOperation = TableOperation.Retrieve<ConsultorioAzureEntity>(id.Substring(0,2),id);
            TableResult retrievedResult = await table.ExecuteAsync(retrieveOperation);

                // Print the phone number of the result.
                if (retrievedResult.Result != null)
                {
                    var r = retrievedResult.Result as ConsultorioAzureEntity;
                    return new ConsultorioIdentity(){
                        Id = r.Id,
                        Numero=r.Numero,
                        Identificador= r.Identificador

                    };
                 }
                else
                {
                    return null;
                 }
        }

       public async Task<List<ConsultorioIdentity>> LeerTodos()
        {
            var table = ObtenerTablaAzure();
            var tk = new TableContinuationToken();
            var query = new TableQuery<ConsultorioAzureEntity>();
            var lista = new List<ConsultorioIdentity>();
            do
            {
                // Retrieve a segment (up to 1,000 entities).
                TableQuerySegment<ConsultorioAzureEntity> tableQueryResult = await
                     table.ExecuteQuerySegmentedAsync(query, tk );

                // Assign the new continuation token to tell the service where to
                // continue on the next iteration (or null if it has reached the end).
                tk = tableQueryResult.ContinuationToken;
                lista.AddRange(tableQueryResult.Results.Select( az => new ConsultorioIdentity(){
                    Id = az.Id,
                   Numero=az.Numero,
                   Identificador=az.Identificador
                     })
                );

                // Print the number of rows retrieved.
 
            // Loop until a null continuation token is received, indicating the end of the table.
            } while(tk != null);
             return lista;

        }
    }

    public class ConsultorioAzureEntity : TableEntity
{
 public ConsultorioAzureEntity(string Id, string Identificador)
    {
        this.PartitionKey = Identificador.Substring(0,2);
        this.Id=Id;
        this.RowKey =Identificador;
    }
 public ConsultorioAzureEntity(){}

        public String Id { get{
            return RowKey;
        }
        set {
            RowKey = value;
        }
        }

         public String Numero { get; set; }

        public String Identificador { get; set; }

     }

}

