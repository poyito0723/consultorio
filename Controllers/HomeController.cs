﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using consulauth.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace consulauth.Controllers
{
    public class HomeController : Controller
    {

        private UserManager<ApplicationUser> userManager;

        public HomeController(UserManager<ApplicationUser> userM)
        {
            userManager = userM;
        }
        
        public IActionResult Index()
        {
         
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }
        [Authorize(Roles="Doctor")]
        
        public async Task<IActionResult> Contact()
        {
            var user = await userManager.GetUserAsync(this.User);

            ViewData["Message"] = $"Your contact page.{user.Especialidad} - {user.Cedula}";

            return View();
        }
       
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
