using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models;
using Repositorios;

namespace ConsultorioFinal.Controllers
{
    public class DoctorController : Controller
    {
        private IDoctoresRepository doctores;
        public DoctorController(IDoctoresRepository doctorRepo)
        {
            doctores = doctorRepo;
        }
        // GET: Doctor
        public async Task<ActionResult> Index()
        {
            
          /*   var model=  doctores.LeerTodos()  
             .Select (c => 
             new Doctor(){ 
                 Id=c.Id,
                 Nombre=c.Nombre,
                 ApellidoP=c.ApellidoP,
                 ApellidoM=c.ApellidoM,
                 Especialidad=c.Especialidad,
                 Horario=c.Horario

             })
                 ;*/
                 var model = from c in await doctores.LeerTodos()
                 select new Doctor(){
                 Id=c.Id,
                 Nombre=c.Nombre,
                 ApellidoP=c.ApellidoP,
                 ApellidoM=c.ApellidoM,
                 Especialidad=c.Especialidad,
                 Horario=c.Horario

                 };
            return View(model);
        }

        // GET: Doctor/Details/5
        public async  Task<ActionResult> Details(String id)
        {
            var model = await doctores.LeerPorId(id);
            return View(new Doctor(){
                Id=model.Id,
                Nombre=model.Nombre,
                ApellidoP=model.ApellidoP,
                ApellidoM=model.ApellidoM,
                Especialidad=model.Especialidad,
                Horario=model.Horario

            });
        }

        // GET: Doctor/Create
        public ActionResult Create()
        {
            var model = new CrearDoctorModel();
            return View(model);
        }

        // POST: Doctor/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async  Task<ActionResult> Create(CrearDoctorModel model)
        {
            if(ModelState.IsValid){
            try
            {
                // TODO: Add insert logic here
               await doctores.Guardar(new DoctorEntity(){
                Nombre=model.Nombre,
                ApellidoP=model.ApellidoP,
                ApellidoM=model.ApellidoM,
                Especialidad=model.Especialidad,
                Horario=model.Horario,
                Id=model.RFC
                });
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
            }
            return View();
        }

        // GET: Doctor/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            var model= await doctores.LeerPorId(id);
            return View(new EditarDoctorModel(){
                Id=model.Id,
                Nombre=model.Nombre,
                ApellidoP=model.ApellidoP,
                ApellidoM=model.ApellidoM,
                Especialidad=model.Especialidad,
                Horario=model.Horario
            });
        }

        // POST: Doctor/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(EditarDoctorModel model)
        {
            if(!ModelState.IsValid){
                return View(model);
            }
            try
            {
                // LLenar datos
                await doctores.Actualizar(new DoctorEntity(){
                Id=model.Id,
                Nombre=model.Nombre,
                ApellidoP=model.ApellidoP,
                ApellidoM=model.ApellidoM,
                Especialidad=model.Especialidad,
                Horario=model.Horario
                });
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Doctor/Delete/5
        public async Task<ActionResult> Delete(String id)
        {
            var model = await doctores.LeerPorId(id);
                
            return View(new BorrarDoctorModel(){
                Id=model.Id,
                Nombre=model.Nombre
                
            });
        }

        // POST: Doctor/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async  Task<ActionResult> Delete(String id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
               await  doctores.Borrar(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}