

using consulauth.Models;
using Microsoft.AspNetCore.Identity;

namespace authsample.Data{

public static class IdentityInitializer{


    public static void SeedRoles(RoleManager<IdentityRole> roleManager){
        if(!roleManager.RoleExistsAsync("Admin").Result){
            var adminRole = new IdentityRole("Admin");
           var res = roleManager.CreateAsync(adminRole).Result;
        }

        if(!roleManager.RoleExistsAsync("Doctor").Result){
            var doctorRole = new IdentityRole("Doctor");
           var res2= roleManager.CreateAsync(doctorRole).Result;

        }


    }

    public static void SeedUsers(UserManager<ApplicationUser> userManager){


            if(userManager.FindByNameAsync("admin@myapp.com").Result == null){
             var adminUser = new ApplicationUser();
             adminUser.UserName = "admin@myapp.com";
             adminUser.Email = "admin@myapp.com";
             adminUser.PhoneNumber ="12345678";
             adminUser.Token ="1234567";
            adminUser.Especialidad="MA";
            adminUser.Cedula="123454657";
            // adminUser.Especialidad="Doctor de doctores";

             var resultado = userManager.CreateAsync(adminUser, "123456789").Result;

             if(resultado.Succeeded){
                 userManager.AddToRoleAsync(adminUser,"Admin");
             }  
            }

        }
    }
}




